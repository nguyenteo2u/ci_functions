def call(repo, imageName) {
    pipeline { 
        agent any 
        parameters {
            booleanParam(defaultValue: false, description: 'Deploy the App', name: 'DEPLOY')
        }

        stages { 
            stage('Build') { 
                steps { 
                    sh 'pip install -r requirements.txt'
                }
            }
            stage('Lint') {
                steps {
                    sh 'pylint-fail-under --fail_under 5.0 *.py'
                }
            }
            stage('Test') {
                steps {
                    script {
                        def files = findFiles(glob: '**/test*.py')
                        for (int i = 0; i < files.size(); ++i) {
                            sh "python3 ${files[i]}"
                            sh "coverage run --omit */site-packages/*,*/dist-packages/* ${files[i]}"
                        }
                        
                        exists = fileExists 'test-reports'
                        if (exists) {
                            junit 'test-reports/*.xml'
                        }
                        
                        exists = fileExists 'api-test-results'
                        if (exists) {
                            junit 'api-test-results/*.xml'
                        }
                        
                        sh "coverage report"
                    }
                }
            }
            stage('Docker') { 
                when {
                    expression { env.GIT_BRANCH == 'origin/master' }
                }    
                steps { 
                    withCredentials([string(credentialsId: 'bubbles16', variable: 'TOKEN')]) {
                        sh "docker login -u 'bubbles16' -p '$TOKEN' docker.io" 
                        sh "docker build -t ${repo}:latest --tag bubbles16/${repo}:${imageName} ." 
                        sh "docker push bubbles16/${repo}:${imageName}" 
                    }  
                }
            }
            stage('Package') {
                steps {
                    sh "zip app.zip *.py"
                    archiveArtifacts artifacts: "app.zip", fingerprint: true
                }
            }
            stage('Deliver') {
                when {
                    expression { params.DEPLOY }
                }
                steps {
                    sh "docker stop ${repo} || true && docker rm ${repo} || true"
                    sh "docker run -d --name ${repo} ${repo}:latest"
                }
            }
        }
    } 
}
